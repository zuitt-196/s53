
// impory the react bootstrap react  hook
import {useState , useEffect, useContext} from 'react';
import {Navigate, Link} from 'react-router-dom';
// impot the useContent 
// import swealert2 
import Swal from 'sweetalert2'
import UserContext from '../UserContext';

import { Form,Button } from "react-bootstrap";


export default function Login() {

    // getting the value of useContent as the distructure 
    //alows us to consume the user cotext and it's  properties  to use for user validation 
    const {user, setUser} = useContext(UserContext)
    console.log(user);




    // use the hooks react and must be import it.
     const[email, setEmail] = useState("")
     const [password, setpasword] = useState("");
      //determine whether submit  button is enabled or not
      const [isActive, setIsActive] = useState(false)
    
    
            // use effect as the validation condition
            useEffect(() => {   
               
                if ((email !== '' &&  password !== '')){
                    setIsActive(true);
                }else {
                    setIsActive(false);
                } 
                // the basis validation
            }, [email, password])


            function loginUser(e){
                e.preventDefault();

                fetch('http://localhost:4000/users/login',{

                method: 'POST',
                headers:{
                        'Content-type': 'application/json'
                }, 
                    body: JSON.stringify({
                        email:email,
                        password:password
                    })

                // making the token evey login user
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    if(typeof data.accessToken !== 'undefined'){
                        localStorage.setItem('token', data.accessToken);

                        //from retrive function to invoke 
                        retrieveUserDeatails(data.accessToken);

                        // put the sweetalert 
                        Swal.fire({
                            title: "Login Successful",
                            icon: "success",
                            text: "Welcome to Booking App of 196"
                        })
                    }else{
                        Swal.fire({
                            title: "Authentication Failed",
                            icon: "error",
                            text: "Check your credentials"
                        })
                    }
                })

                 // set the email of the login User in the local; storage   
                    //  
                // localStorage.setItem("email", email);

                // Acces user information  through localStorage 
                // 
                // setUser ({
                //     email: localStorage.getItem("email")
                // })

                setEmail('');
                setpasword('');
               alert(`${email}thank you for regiter`);
    
            };

            // GET retrive the user 
            const retrieveUserDeatails = (token)=>{

                fetch('http://localhost:4000/users/getUserDetails',{
                    headers:{
                        Authorization: `Bearer ${token}`
                    }

                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    setUser({
                        id:data._id,
                        isAdmin: data.isAdmin
                    });
                })
            }
    

              
    return(
        // ternary condition
        (user.id !==null)?
            <Navigate to="/Course"/>
        :

    

        <>
              <h1>Log in</h1>
        
         <Form onSubmit={e => loginUser(e)}>    
                   <Form.Group>
                               <Form.Label>Email Address</Form.Label>
                               <Form.Control  type ="Email" placeholder="Enter your email address" required 
                                   value = {email} onChange = {e => setEmail(e.target.value)}/>
                               <Form.Text className="text-muted">
                                       We' ll never share emails with others
                               </Form.Text>
                   </Form.Group>

                
                   <Form.Group controlId = "password1">
                               <Form.Label>password</Form.Label>
                               <Form.Control type = "password" placeholder = "Enter your password" required   value = {password} onChange = {e => setpasword(e.target.value)}/>
                   </Form.Group>
                   <p>Not yet registred? <Link to = "/Rigister">Reggister Here</Link></p>
                   
                {isActive ? 
                  <Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">
                  Login
                </Button>
  
                   :
                   <Button className ="mt-3 mb-5" variant ="danger" type = "submit" id ="submitBtn" disabled>Login</Button>

                   }
         </Form>  
      </>

   )
     




}