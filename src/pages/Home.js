import Banner from '../components/Banner';
import Highlights from '../components/Highlights'
 
export default function home() {
    const data = {
        Title: "Booking-App 196",
        content: "Enroll the course that we offer",
        destination:"", ///-> notice port  route
        label:"Back Home"

    }

    return (
        <>
            <Banner data = {data}/>
            <Highlights/>
        </>
    )
}