
import { useState, useEffect, useContext,} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){

	const {user, setUser} = useContext(UserContext);
    const history  = useNavigate();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password, setPassword] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// check if values are succesfully binded
	// console.log(email);
	// console.log(password1);
	// console.log(password2);


	function registerUser(e){
		// prevents page redirection via form submission
		e.preventDefault();

				fetch('http://localhost:4000/users/checkEmailExists', {
		    method: "POST",
		    headers: {
		        'Content-Type': 'application/json'
		    },
		    body: JSON.stringify({
		        email: email
		    })
		})
		.then(res => res.json())
		.then(data => {

		    // console.log(data);

		    	if(data === true){
					Swal.fire({
		    			title: 'Duplicate email found',
		    			icon: 'error',
		    			text: 'Kindly provide another email to complete the registration.'	
		    		});
		    	}else {

		                fetch('http://localhost:4000/users', {
		                    method: "POST",
		                    headers: {
		                        'Content-Type': 'application/json'
		                    },
		                    body: JSON.stringify({
		                        firstName: firstName,
		                        lastName: lastName,
		                        email: email,
		                        mobileNo: mobileNo,
		                        password: password
		                    })
		                })
		                .then(res => res.json())
		                .then(data => {

		                    console.log(data);

		                    if (data) {

		                       Swal.fire({
		                            title: 'Duplicate email found',
		                            icon: 'info',
		                            text: "The email that you're trying to register is already exist"

		                       
		                        });
		                        // history("/login");

		                    } else {
                                fetch('http://localhost:4000/users',
                                            {
                                method: 'POST',
                                headers: {
                                        'Content-Type': 'application/json'
                                },
                                body:JSON.stringify({
                                    firstName: firstName,
                                    lastName: lastName,
                                    email: email,
                                    mobile: mobileNo,
                                    password:password
                                })
                                })
                                .then(res => res.json())
                                .then(data=>{
                                    console.log(data);

                                    if(data.email){
                                        Swal.fire({
                                            title: 'Registration Succesfull',
                                            icon : ' Sucess',
                                            text: 'Thank yun for registering'
                                        })

                                        history("/Login");
                                    }else{
                                        Swal.fire({
                                            title: 'Registration Failed',
                                            icon : ' error',
                                            text: 'Something went wrong, try again'
                                        })
                                    }
                                })
                                
							
		                    }
		                })
		            };

		})


		// clear input field

		 setFirstName('');
		 setLastName('');
		 setEmail('');
		 setMobileNo('');
		 setPassword('');

		// alert('Thank you for registering');

	}

	// syntax:
		// useEffect (() => {}, [])

	useEffect (() => {

		if ((firstName !== ''&& lastName !== '' && mobileNo !== '' && email !== '' && password !== '' && mobileNo.length === 11)){
			setIsActive(true);
		}else {
			setIsActive(false);
		}


	}, [firstName,lastName,mobileNo,email,password])



	return(
		(user.id !== null) ?
			<Navigate to="/courses" />
		:
		<Form onSubmit={(e) => registerUser(e)}>
		  <h1>Register</h1>
		  <Form.Group controlId="firstName">
		      <Form.Label>First Name:</Form.Label>
		      <Form.Control 
		          type="text" 
		          placeholder="Please input your first name here"
		          value={firstName} 
		          onChange={e => setFirstName(e.target.value)}
		          required
		      />
		  </Form.Group>

		  <Form.Group controlId="lastName">
		      <Form.Label>Last Name:</Form.Label>
		      <Form.Control 
		          type="text" 
		          placeholder="Please input your last name here"
		          value={lastName} 
		          onChange={e => setLastName(e.target.value)}
		          required
		      />
		  </Form.Group>

		  <Form.Group controlId="mobileNo">
		      <Form.Label>Mobile Number:</Form.Label>
		      <Form.Control 
		          type="text" 
		          placeholder="Please input your 11-digit mobile number here"
		          value={mobileNo} 
		          onChange={e => setMobileNo(e.target.value)}
		          required
		      />
		  </Form.Group>

			<Form.Group className="mb-3" controlId="userEmail">
		    <Form.Label>Email address:</Form.Label>
		    <Form.Control 
		    	type="email" 
		    	placeholder="Please input your email here" 
		    	value={email}
		    	onChange={e => setEmail(e.target.value)}
		    	required
		    />
		    <Form.Text className="text-muted">
		      We'll never share your email with anyone else.
		    </Form.Text>
		  </Form.Group>

		  <Form.Group className="mb-3" controlId="password">
		    <Form.Label>Password:</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Please input your password here" 
		    	value={password}
		    	onChange={e => setPassword(e.target.value)}
		    	required
		    />
		  </Form.Group>
		  
		  {/* Conditionally render submit button based on isActive state */}
		  { isActive ?
		  		<Button variant="primary" type="submit" id="submitBtn">
		  			  Register
		  		</Button>
		  	:
			  	<Button variant="danger" type="submit" id="submitBtn" disabled>
			  	  Register
			  	</Button>
		  }
		  
		</Form>

		)
}




















































































































// // everytime we create an page make sure you indicate the export 
// // importing the hooks
// import {useState , useEffect ,useContext} from 'react';

// // import the Navigate from react-router-dom
// import {Navigate} from 'react-router-dom';

// import UserContext from '../UserContext';

// // import Swal from 'sweetalert';

// import { Form,Button } from "react-bootstrap";

// export default function Register(){

//     const {user, setUser} = useContext(UserContext)
//     console.log(user);


//     // const[email, setEmail] = useState("")
//     //  const [password1, setpasword1] = useState("");
//     //  const [password2, setpasword2] = useState("");
    
//     const [firstName, setFirstName] = useState("")
//     const [lastName, setLastName] = useState("")
//     const [mobileNo, setMobileNo] = useState("")
//     const [email, setEmail] = useState("")
//     const [password, setPassword] = useState(false)

     
//      //determine whether submit  button is enabled or not
//      const [isActive, setIsActive] = useState(false)


//         //  check if values are successfully binded
//             // console.log(email);
//             // console.log(password1);
//             // console.log(password2);
//             //Syntax
//                     // useEffect (() => {}, [])

//         function registerUser(e){
//             e.preventDefault();

//             // setEmail('');
//             // setPassword('');
            

//             // fetch('http://localhost:4000/users/checkEmailExists', {
//             // method: 'POST',
//             // headers :{
//             //         'Content-type :application/json'
//             // },
//             //     body : JSON.stringify({
//             //         email : email
//             //     })


//             // })
//             // .then(res => res.json())
//             // .then(data => {
//             //     console.log(data);
//             //     // reslult // bolean 

//             //    if(data){
//             //     Swal.fire ({
//             //         title: "Duplicate email found",
//             //         icon: "info",
//             //         text: "the email that you are trying to register already exist"
//             //     })
//             //    } else {
//             //        fetch('http://localhost:4000/users') 

//             //    }
//             // })


        
//             //set the email of the Rigester in the local storage
//              localStorage.setItem('email',email)

//             setUser({
//                 email: localStorage.getItem("email")
//             })

//                 alert("thank you for register")

//                 // Acces Rigesteruser information  through localStorage 
//         }
//         // to make validation to an apllicattion used by useEffect
//             useEffect(() => {   
//                 // validation to enabled the sumbit button when all the fields are populated and both password match
//                 if ((email !== '' && firstName !== '' && lastName !== '' && password !== '' && mobileNo !== '' && mobileNo.length === 11)){
//                     setIsActive(true);
//                 }else {
//                     setIsActive(false);
//                 }
//             }, [email,firstName,lastName, mobileNo, password])
//         return(
//             //Ternary condition within the  Navigate react route
//             // change the user.email into user.id sinde they already convert into  token 
//                 (user.id !== null)?
//                 <Navigate to = "/Course"/>
//                 :
//              <>
//                    <h1>Register Here:</h1>
             
//               <Form onSubmit={e => registerUser(e)}>    
//                         <Form.Group>

//                         <Form.Group controlId = "firtsNmae">
//                                     <Form.Label>Firts Name</Form.Label>
//                                     <Form.Control type = "text" placeholder = "Enter your FirstName" required   value = {firstName} onChange = {e => setFirstName(e.target.value)}/>
//                         </Form.Group>


//                         <Form.Group controlId = "lastNmae">
//                                     <Form.Label>Last Name</Form.Label>
//                                     <Form.Control type = "text" placeholder = "Enter your LastName" required   value = {lastName} onChange = {e => setLastName(e.target.value)}/>
//                         </Form.Group>


                            
//                         <Form.Group controlId = "mobile">
//                                     <Form.Label>Mobile Number</Form.Label>
//                                     <Form.Control type = "text" placeholder = "Enter your Mobile" required   value = {mobileNo} onChange = {e => setMobileNo(e.target.value)}/>
//                         </Form.Group>


//                                     <Form.Label>Email Address</Form.Label>
//                                     <Form.Control  type ="Email" placeholder="Enter your email address" required 
//                                         value = {email} onChange = {e => setEmail(e.target.value)}/>
//                                     <Form.Text className="text-muted">
//                                             We' ll never share emails with others
//                                     </Form.Text>
                                    
//                         </Form.Group>
                        
//                         <Form.Group controlId = "password">
//                                     <Form.Label>password</Form.Label>
//                                     <Form.Control type = "password" placeholder = "Enter your password" required   value = {password} onChange = {e => setPassword(e.target.value)}/>
//                         </Form.Group>

        
//                      {isActive ?      
//                          <Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">  Registe</Button>
//                       :
//                         <Button className ="mt-3 mb-5" variant ="danger" type = "submit" id ="submitBtn" disabled>Register</Button>

//                         }
//               </Form>  
//            </>

//         )

// }