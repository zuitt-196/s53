// import data 
import {useState ,useEffect} from 'react'

import coursesData from '../data/coursesData'
import CourseCard from '../components/CourseCard' 

export default function Coures() {
    // console.log(coursesData);
    // console.log(coursesData[0])

    const [course , setCourses] = useState([]);
    // getting the database from the backend used the fetch
    useEffect(()=>{
        fetch('http://localhost:4000/courses')
        .then(res => res.json())
        .then(data => {
                console.log(data);

                setCourses(data.map(course =>{
                      return (
                        <CourseCard key={course._id} courseProp ={course}/>
                      )  

                }))
        })


    },[])

    // to manipulate the array used the map iterate from the mock up data or to take it inside the components of coursCards
    // as anoymous function make parameter which is course (singular)
    // const courses= coursesData.map(course =>{
    //         return (
    //             // by used the key as the unique every loop 
    //             <CourseCard key={course.id} courseProp ={course}/>
    //         )

    // })
    return (
        <>
             <h1>Available</h1>
             {course}
        
        </>
       

    )


}