import { Card , Container,Row, Col, Button} from "react-bootstrap";

import {useState, useEffect, useContext} from 'react'
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from "../UserContext";
import Swal from 'sweetalert2';

export default function CouresView (){

    const {user} = useContext(UserContext);
    // allows us to gain access to methods that will allowd us to rediret a user to as deffernt page after enrolling a course 
    const history =- useNavigate()
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [Price, setPrice] = useState(0);

    // useparams hook allowds use to retrev
    const {courseId} = useParams("course");

    const enroll =(courseId) =>{

        fetch('http://localhost:4000/users/enroll',{
            method: 'POST',
            headers: {'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    
                    },
                        body: JSON.stringify({
                            courseId : courseId
                        })
                        
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data);

            if (data) {
                Swal.fire({
                     title: 'Sucessfully enrolled',
                     icon:'success',
                     text: 'thank you for enrolling'
                })

                history("/courses")
            }else {
                Swal.fire({
                    title: 'Sucessfully went wronf',
                    icon:'error',
                    text: 'Please try again later'
                })
            }
        })
    };

    useEffect(() => {
        console.log(courseId)
        fetch(`http://localhost:4000/courses/getSingleCourse/${courseId}`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            // to set  the name , description and setPrice
            setName(data.name);
            setDescription(data.description);
            setPrice(data.setPrice);
        })
    },[courseId])



    return (
        <Container className = "mt-5">
            <Row>
                    <Col lg={{span:6, offset:3}}>
                        <Card.Body>
                               <Card.Title>{name}</Card.Title> 
                               <Card.Subtitle>Description:</Card.Subtitle>
                               <Card.Text>{description}</Card.Text>
                               <Card.Subtitle>Price:</Card.Subtitle>
                               <Card.Text>{Price}</Card.Text>
                               <Card.Text>Class Schedule</Card.Text>
                               <Card.Text>8:00 Am to 5:00 PM</Card.Text> 
                               { user.id !==null? 
                               <Button variant = "primary" onclick={() => enroll(courseId)}>Enroll</Button> 

                                : 
                                    <Link className="btn btn-danger" to="/Login" >Login</Link>
                                }
                        </Card.Body>

                    </Col>

            </Row>

        </Container>
    )

}